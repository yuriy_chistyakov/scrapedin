const openPage = require('../../helpers/openPage')
const scrollToPageBottom = require('../../helpers/scrollToPageBottom')

const logger = require('../../helpers/logger')

module.exports = async (browser, url, waitTimeToScrapMs = 500) => {
  logger.info('Company feed', `starting scraping url: ${url}`)

  const page = await openPage(browser, url)
  const companyIndicatorSelector = '.updates__main'

  await page.waitFor(companyIndicatorSelector, { timeout: 5000 })
    .catch(() => {
      throw new Error('linkedin: copany not found')
    })

  await scrollToPageBottom(page, 5)

  if (waitTimeToScrapMs) {
    logger.info('company', `applying 2nd delay`)
    await new Promise((resolve) => { setTimeout(() => { resolve() }, waitTimeToScrapMs) })
  }

  const feedObjects = []

  const feeds = await page.$$('.share-update-card.feed-card')
  for (let feed of feeds) {
    const date = await feed.$eval('.feed-card__post-date', node => node.innerHTML);
    const content = await feed.$eval('.share-update-card__update-text', node => node.innerHTML)
    const feedObject = {
      date,
      content
    }
    const feedArticle = await feed.$('.share-article')
    const feedVideo = await feed.$('.share-native-video')
    const feedImages = await feed.$('.share-images')
    if (feedArticle) {
      feedObject.sharedArticle = await feed.$eval('.share-article', node => node.innerHTML)
    }
    if (feedVideo) {
      feedObject.videoSrc = await feed.$eval('.share-native-video video', node => node.getAttribute('src'))
    }
    if (feedImages) {
      feedObject.images = await feed.$eval('.share-images', node => node.outerHTML)
    }
    feedObjects.push(feedObject)
  }
  const rawHtml = await page.$eval('.feed', node => node.outerHTML)
  const cssPage = await openPage(browser, 'https://static-exp1.licdn.com/sc/p/com.linkedin.organization-guest-frontend%3Aorganization-guest-frontend-static-content%2B0.1.413/f/%2Forganization-guest-frontend%2Fstylesheets%2Fcompany%2Fdesktop%2Foverview_en_US.css')
  const css = await cssPage.$eval('body', node => node.innerText)
  return { feedObjects, html: rawHtml, css }
}
