const puppeteer = require('puppeteer')
const login = require('./helpers/login')
const profile = require('./strategies/profile/profile')
const companyFeed = require('./strategies/company-feed/company-feed')
const logger = require('./helpers/logger')

const getScrapperStrategy = (browser, url, waitMs) => {
  if (url.includes('company')) {
    return companyFeed(browser, url, waitMs)
  } else if (url.includes('in')) {
    return profile(browser, url, waitMs)
  } else {
    throw new Error('Unknown page')
  }
}

module.exports = async ({ email, password, isHeadless, hasToLog, puppeteerArgs } = { isHeadless: true, hasToLog: false }) => {
  if (!hasToLog) {
    logger.stopLogging()
  }
  logger.info('scrapedin', 'initializing')

  if (!email || !password) {
    logger.warn('scrapedin', 'required parameters email and password was not provided')
  } else {
    logger.info('scrapedin', 'required parameters email and password was provided')
  }

  const args = Object.assign({ headless: isHeadless }, puppeteerArgs)
  const browser = await puppeteer.launch(args)

  if (email && password) {
    try {
      await login(browser, email, password, logger)
    } catch (e) {
      await browser.close()
      throw e
    }
  }

  return async (url, waitMs) => {
    const result = await getScrapperStrategy(browser, url, waitMs)
    await browser.close()
    return result
  }
}
